json.array!(@videos) do |video|
  json.extract! video, :id, :name, :url, :lat, :lon, :view
  json.url video_url(video, format: :json)
end

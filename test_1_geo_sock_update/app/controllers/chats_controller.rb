require 'digest/md5'

class ChatsController < ApplicationController
  def room
    redirect_to login_path unless session[:username]
    # By Default. User will join in to public room
    @uniqueRoomAddr = "/messages/public"
    @roomName = "Public Room"
    # If chatmate nickname is defined... Create a room with secret Channel.
    if(params.has_key?(:nickname))
      chatmate = params[:nickname]
      username = session[:username]
      uniqueName = [chatmate, username].sort().join("_");
      # This solution still under repeat attack
      @uniqueRoomAddr = "/messages/" + Digest::MD5.hexdigest(uniqueName + "umbala_abalum").to_s
      @roomName = "private room with " + chatmate;  
    end
    @domain = request.host
  end
end

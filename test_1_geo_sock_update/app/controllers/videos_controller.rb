class VideosController < ApplicationController
  
  include VideosHelper
  
  before_action :set_video, only: [:show, :edit, :update, :destroy]
  
  # init realtime controller using redis - realtime gem
  realtime_controller({:queue => :redis}) # instruct all requests to enable realtime support via redis
  
  
  #
  # We need override this function (in gem realtime) for only send update notify to target 
  # that view video with video_id (not all viewers)
  #
  def store_realtime_session_redis
      # store session data or any authentication data you want here, generate to JSON data
      old_session_data_str = RedisWrapper.redis.hget("rtSession-" + realtime_user_id.to_s, @realtime_token)
      video_id = (@video_is_loading_id || -1).to_i
      session_data = {
            "user_id" => realtime_user_id,
      }
      
      if !old_session_data_str.nil?
        old_session_data = JSON.parse(old_session_data_str)
      end
      
      
      session_data[:video_ids] = if old_session_data_str.nil? || 
                                     !old_session_data.kind_of?(Hash) ||
                                     old_session_data.key?(:video_ids) ||
                                     !old_session_data["video_ids"].kind_of?(Array) 
                                      [video_id,]
                                    else
                                      old_session_data["video_ids"].push(video_id)
                                    end
      

      # todo: merge additional session data passed in
      
      stored_session_data = JSON.generate(session_data)

      RedisWrapper.redis.hset(
        "rtSession-" + realtime_user_id.to_s,
        @realtime_token,
        stored_session_data,
      )
      
      # expire this realtime session after one day.
      RedisWrapper.redis.expire("rtSession-" + realtime_user_id.to_s, 86400)
  end
  
  def map
    # find the video
    @video_is_loading_id = params[:id]
    @video = Video.find(params[:id].to_i)
  end

  # GET /videos
  # GET /videos.json
  def index
    @videos = Video.all
  end

  # GET /videos/1
  # GET /videos/1.json
  def show
    
    # check if this viewer watch this video before ?!
    unique_viewer = is_unique_viewer @video.id
    
    # location check
    latitude = (params[:lat] || 45.0).to_f
    longitude = (params[:lon] || 4.0).to_f
    
    
    @video_is_loading_id = @video.id
    @video.view += 1
    @video.unique_view = (@video.unique_view || 0) + 1 if unique_viewer  # we should sure unique value not nil value
    @video.save()
    
    # notify to realtime server to push notification to viewer who is viewing this video.
    $redis.publish 'realtime_msg', {msg: "video_views_increase", unique_viewer: unique_viewer, lat: latitude, lon: longitude, recipient_user_ids: [42,], video_id: @video.id}.to_json
  end

  # GET /videos/new
  def new
    @video = Video.new
  end

  # GET /videos/1/edit
  def edit
  end
  

  def realtime_user_id
    return 42 # if using devise, change this to current_user.id
  end

  
  def realtime_server_url
    # point this to your node.js-socket.io-redis/zmq realtime server (you can set this later)
    @realtime_server_url = 'http://localhost:5001'
    return 'http://localhost:5001'
  end

  # POST /videos
  # POST /videos.json
  def create
    @video = Video.new(video_params)

    respond_to do |format|
      if @video.save
        format.html { redirect_to @video, notice: 'Video was successfully created.' }
        format.json { render :show, status: :created, location: @video }
      else
        format.html { render :new }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /videos/1
  # PATCH/PUT /videos/1.json
  def update
    respond_to do |format|
      if @video.update(video_params)
        format.html { redirect_to @video, notice: 'Video was successfully updated.' }
        format.json { render :show, status: :ok, location: @video }
      else
        format.html { render :edit }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video.destroy
    respond_to do |format|
      format.html { redirect_to videos_url, notice: 'Video was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_params
      params.require(:video).permit(:name, :url, :lat, :lon, :view)
    end
end

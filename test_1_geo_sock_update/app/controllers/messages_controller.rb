class MessagesController < ApplicationController
  include MessagesHelper
  
  before_action :set_message, only: [:show, :edit, :update, :destroy, :read]
  skip_before_filter :verify_authenticity_token


  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.all
  end

  # Client will defined when it read or not ?! 
  # That's mean Show is not read.
  # We need Id of read message to process it
  # GET /messages/read/10.json
  # POST /messages/read/10.json
  def read
    message_id = params[:id]
    read = @message.read
    unless @message.read
      @message.read = true
      @message.expired = DateTime.now + 12.hours
      @message.save
    end    
    
    respond_to do |format|
      if !read
        format.html { redirect_to @message, notice: 'Accepted.' }
        format.json { head :no_content }
      else
        format.html { redirect_to @message, notice: 'Accepted.' }
        format.json { head :no_content }
      end
    end
    
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
  end

  # GET /messages/new
  def new
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  # POST /messages.json
  def create
    # destroy_all
    # When we create new message that same with reply ... So delete all chatmate message.
    delete_chatmate_message message_params
    create_params = init_message_record message_params
    @message = Message.new(create_params)

    respond_to do |format|
      if @message.save
        format.html { redirect_to @message, notice: 'Message was successfully created.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    respond_to do |format|
      if @message.update(message_params)
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:from, :to, :content, :expired, :read, :reply)
    end
end

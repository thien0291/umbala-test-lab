=begin
What will we save?!
We will save user data under redis with HASH table. Each user has a key in redis server define under pattern u_username
each u_username is a hash table, it contain message a list of massages of owner with other one. Exp:

u_Alex  |--> u_Jone [m_1437103777, m_1437103989]
        |--> u_Maria [m_1437103989]
        |--> u_Ozawa [m_1437103989]
                
We can reach to message by pattern message_from_to_unixtime (which define in above struct.)

message_u_Alex_u_Jone_1437103777 --> {content: "message 0", read: false}  /* Expire: 10000 */
message_u_Alex_u_Jone_1437103989 --> {content: "message 1", read: false}  /* Expire: 6969 */
message_u_Alex_u_Maria_1437103989 --> {content: "message 2", read: true}  /* Expire: 9696 */ 
message_u_Alex_u_Ozawa_1437103989 --> {content: "message 3", read: false} /* Expire: 9669 */

*Notice: Redis only check expired key but not for hash key.

We should has a sign to recognize this message read or not but no need to pull that massage out.
so when message is readed... We will rename first char from m (message) --to--> rm (read_message)

=end

class RedisMessagesController < ApplicationController
  
  include RedisMessagesHelper
  skip_before_filter :verify_authenticity_token



  # POST /redis-message/read/
  # Param "reader" "sender"
  def read
    respond_to do |format|
      if read_messsage params["reader"], params["sender"]
        format.json { render json: JSON.generate({status: "OK"}), status: :accepted }
      else
        format.json { render json: JSON.generate({status: "ERROR"}), status: :unprocessable_entity }
      end
    end
    
  end

  # GET /redis-messages/m_teo_ti_1437120996
  # GET /redis-messages/m_teo_ti_1437120996.json
  def show
    msg_key = params["key"]
    msg_content = $redis.get(msg_key)
    puts msg_content
    respond_to do |format|
      if !msg_content.empty? && !msg_content.nil? 
        format.json { render json: msg_content, status: :created }
      else
        format.json { render json: JSON.generate({status: "ERROR"}), status: :unprocessable_entity }
      end
    end
    
  end

  # POST /redis-messages
  def create
    # When we create new message that same with reply ... So delete all chatmate message.
    reply_message params["from"], params["to"]
    msg_key = save_message params["from"], params["to"], params["content"]  
    # Create new Message    
    respond_to do |format|
      if msg_key != false
        format.json { render json: JSON.generate({status: "OK", key: msg_key}), status: :created }
      else
        format.json { render json: JSON.generate({status: "ERROR"}), status: :unprocessable_entity }
      end
    end
  end


  # DELETE /redis-messages/m_teo_ti_1437120996
  # DELETE /redis-messages/m_teo_ti_1437120996.json
  def destroy
    $redis.del(params["key"])
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def redis_message_params
      params.permit(:from, :to, :replier, :sender, :id, :key)
    end
    
end

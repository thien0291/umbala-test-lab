// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

function blinkIt(target){	
	target.addClass("blink-it");
	setTimeout(function(){
    	target.removeClass("blink-it");
  	}, 1000);
}

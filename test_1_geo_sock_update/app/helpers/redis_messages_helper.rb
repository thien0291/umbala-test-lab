module RedisMessagesHelper
  # replace 12 day = 20 sec and 12 hour = 5 sec ... we need it for stress test.
  def save_message(from, to, content)
    # save message to redis with key pattern: message_u_Alex_u_Jone_1437103777
    current_unix_time = Time.now.to_i.to_s
    message_key = "m_#{from}_#{to}_#{current_unix_time}"
    content_value = {content: content}  #, read: false} for performance ... we check read message in ref_key
    $redis.set(message_key, JSON.generate(content_value));
    $redis.expire(message_key, 20)                         # by default set expired after 12 days. 
    
    # Get current message list and append it.
    # put message to "receiver mail box"
    current_messages = $redis.hget("u_#{to.to_s}", "u_#{from.to_s}")
    if current_messages.nil?            # Not exist yet.
      new_message_list = [message_key, ]
    else
      new_message_list = JSON.parse(current_messages)
      new_message_list.push(message_key)
    end
    
    new_message_list = JSON.generate(new_message_list)
    $redis.hset("u_#{to.to_s}", "u_#{from.to_s}", new_message_list)
    $redis.expire("u_#{to.to_s}", 20)                         # extend ttl of this messagebox to 12 days (1036800). 


    # In this time ... by default we return true value.
    message_key  
    
  end
  
  
  
  
  # In this case ... just extend message which unread to 12 hours although 12 days expired reached or not
  # all read message will start with rm... 
  def read_messsage(reader, sender)
    current_messages = $redis.hget("u_#{reader.to_s}", "u_#{sender.to_s}");
    
    unless current_messages.nil?
      current_messages = JSON.parse(current_messages)
      
      messages_need_expired = []      # We need set new expire for each message in this list.
      
      current_messages.each_with_index do |message, index|
        unless message[0] == "r"
          messages_need_expired.push(message) 
          current_messages[index] = "r" + message
        end
      end
      
      messages_need_expired.each do |message|
        $redis.expire(message, 5) # Expire in next 12 hours FROM NOW.
      end
      
      # update current_messages
      current_messages = $redis.hset("u_#{reader.to_s}", "u_#{sender.to_s}", JSON.generate(current_messages));      
    end
    true    # in this time ... it's true again. 
  end
  
  
  
  
  # This is ultility function help us get real key after it's marked (like read_message will make a sign "r" at beginning)
  def get_real_key(key_with_sign)
    marked_words = ["r", ]
    return key_with_sign[1..-1] if marked_words.include?(key_with_sign[0])
    key_with_sign 
  end
  
  
  
  
  
  # We will delete all message from current "receiver" to replier. 
  def reply_message(replier, receiver)
    # Load current messages
    current_messages = $redis.hget("u_#{replier.to_s}", "u_#{receiver.to_s}");
    unless current_messages.nil?
      current_messages = JSON.parse(current_messages)
    
      # delete all
      current_messages.each do |message|
        $redis.del get_real_key(message)
      end
      # Delete pointer      
      $redis.hdel("u_#{replier.to_s}", "u_#{receiver.to_s}")
    end
  end
  
  
  
  
end

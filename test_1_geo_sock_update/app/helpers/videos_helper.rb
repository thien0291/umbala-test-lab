module VideosHelper
    
  # We will check if this is unique visitor and put some information for later check.
  def is_unique_viewer(current_video_id)
    last_view = JSON.parse(cookies[:last_view].nil? ? "[]" : cookies[:last_view]);
    
    # Check if this user viewed this video ? if not then add video_id to viewed list.
    if last_view.kind_of?(Array) && last_view.include?(current_video_id.to_i)
      return false
    else
      if !last_view.kind_of?(Array)                         # for the first time init or not expected value.
        last_view = [current_video_id.to_i]
      else                                                  # if all okie, just add it.
        last_view = last_view.push(current_video_id.to_i)
        last_view.shift if last_view.count > 100            # we only save last 100 viewed videos
      end
    end
    
    # Unique it? No need because if it include in last view list... it will be returned from above code block
    cookies[:last_view] = JSON.generate(last_view)
    return true
    
  end
  
end
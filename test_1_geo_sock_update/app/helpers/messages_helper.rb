module MessagesHelper
  
  def init_message_record message_params
    create_params = message_params.clone
    create_params[:expired] = DateTime.now + 12.days
    create_params[:read] = false
    create_params[:reply] = false
    
    create_params
  end
  
  
  def delete_chatmate_message message_params
    chat_mate_id = message_params[:to]
    chatter = message_params[:from]
    Message.destroy_all(from: chat_mate_id, to: chatter) 
    
  end
  
end

class Message < ActiveRecord::Base
  
  def self.clear_expired_message
      Message.where("expired <= (DATETIME('now'))").destroy_all
  end
  
end

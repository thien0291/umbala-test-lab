class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :from
      t.string :to
      t.string :content
      t.timestamps :expired
      t.boolean :read
      t.boolean :reply

      t.timestamps null: false
    end
  end
end

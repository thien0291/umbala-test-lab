class AddUniqueViewToVideos < ActiveRecord::Migration
  def up
    add_column :videos, :unique_view, :int
  end
  
  def down
    remove_column :videos, :unique_view, :int
  end
end

class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :name
      t.string :url
      t.float :lat
      t.float :lon
      t.integer :view

      t.timestamps null: false
    end
  end
end

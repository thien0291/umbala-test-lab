class AdddExpiredToMessages < ActiveRecord::Migration
  def up
    add_column :messages, :expired, :datetime
  end
    
  def down
    remove_column :messages, :expired, :datetime
  end
end

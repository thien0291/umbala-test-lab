class ChangeUniqueViewToVideos < ActiveRecord::Migration
  def up
    change_column_default :videos, :unique_view, 0
  end
  
  def down
    change_column_default :videos, :unique_view, nil
    drop_table :stories
  end
end

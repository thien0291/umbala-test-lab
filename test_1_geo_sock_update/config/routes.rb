Rails.application.routes.draw do
  get 'resumes/index'
  get 'resumes/new'
  get 'resumes/create'
  get 'resumes/destroy'
  get 'chats/room'
  get 'sessions/new'
  get 'sessions/create'

  # resources :videos
  get "videos/map"
  
  # read - Message API
  get "messages/read/:id(.:format)" => "messages#read"
  post "messages/read/:id(.:format)" => "messages#read"
  
  
  # Realtime Chat message.
  get '/login' => 'sessions#new'#, #:as => :login
  post '/login' => 'sessions#create'#, #:as => :login
  
  get '/chatroom' => 'chats#room'#, :as => :chat
  post '/chatroom' => 'chats#room'#, :as => :chat
  
  
  # Message with redis.
  get '/redis-messages/:key(.:format)' => 'redis_messages#show'
  delete '/redis-messages/:key(.:format)' => 'redis_messages#destroy'
  post '/redis-messages' => 'redis_messages#create'
  post '/redis-messages/read' => 'redis_messages#read'

  
  resources :messages
  resources :videos
  resources :resumes, only: [:index, :new, :create, :destroy]
  resources :media_contents, only: [:create]
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'chats#room'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
